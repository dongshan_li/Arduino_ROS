#if ARDUINO >= 100
 #include "Arduino.h"
 #include "Print.h"
#else
 #include "WProgram.h"
#endif

#include "ks103.h"
#include <Wire.h>

#define KS103ADDR 0xD4    // Address of the 0xe8  选择串口地址，共有20个

KS103::KS103()
{

}

void KS103::powerOn(int address)
{
  Wire.beginTransmission(address);             // Start communticating with KS103
  Wire.write(0x02);                               // Send Reg
  Wire.write(0x71);  // Send 0x72 to set USB Power 三级降噪
  Wire.endTransmission();
}

void KS103::KS103_WriteOneByte(int address, byte reg, byte command)
{
    Wire.beginTransmission(address); // start transmission to device
    Wire.write(reg);             // send register address
    Wire.write(command);                 // send value to write
    Wire.endTransmission();         // end transmission
}

word KS103::KS103_ReadFrom(int address, byte reg)
{
	word _buff = 0;
    Wire.beginTransmission(address); // start transmission to device
    Wire.write(reg);             // writes address to read from
    Wire.endTransmission();         // end transmission

    Wire.beginTransmission(address); // start transmission to device
    Wire.requestFrom(address, 2);    // request 1 bytes from device
    while(Wire.available())         // device may send less than requested (abnormal)
    {
	  _buff <<= 8;
      _buff += Wire.read();       // receive a byte
    }
    Wire.endTransmission();         // end transmission
	return _buff;
}

word KS103::getRange(int address)
{
    word range;
    KS103_WriteOneByte(address,0x02,0xB0);
        delay(10);
    range = KS103_ReadFrom(address,0x02);
    return range;
}
