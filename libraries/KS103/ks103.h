#if ARDUINO >= 100
 #include "Arduino.h"
 #include "Print.h"
#else
 #include "WProgram.h"
#endif
#ifndef KS103_H
#define KS103_H

#define KS103_ADDR_D0	0x68
#define KS103_ADDR_D2	0x69
#define KS103_ADDR_D4	0x6A
#define KS103_ADDR_D6	0x6B
#define KS103_ADDR_D8	0x6C
#define KS103_ADDR_DA	0x6D
#define KS103_ADDR_E0	0x70
#define KS103_ADDR_E2	0x71
#define KS103_ADDR_E4	0x72
#define KS103_ADDR_E6	0x73
#define KS103_ADDR_E8	0x74
#define KS103_ADDR_EA	0x75

class KS103
{
public:
    KS103();
	void powerOn(int address);
    word getRange(int address);

private:
    void KS103_WriteOneByte(int address,byte reg,byte command);
	word KS103_ReadFrom(int address, byte reg);
};

#endif // KS103_H
